const passwordInput = document.querySelector('#password-input');
const confirmPasswordInput = document.querySelector('#confirm-password-input');
const passwordIcons = document.querySelectorAll('.icon-password');
const errorMessage = document.querySelector('.error-message');
const modal = document.querySelector('#modal');
const modalClose = document.querySelector('.modal-close');

passwordIcons.forEach(icon => {
  icon.addEventListener('click', () => {
    const inputField = icon.previousElementSibling;
    if (inputField.type === 'password') {
      inputField.type = 'text';
      icon.classList.add('icon-password-show');
      icon.classList.remove('icon-password-hide');
    } else {
      inputField.type = 'password';
      icon.classList.add('icon-password-hide');
      icon.classList.remove('icon-password-show');
    }
  });
});

document.querySelector('.password-form').addEventListener('submit', event => {
  event.preventDefault();
  if (passwordInput.value === confirmPasswordInput.value) {
    modal.style.display = 'block';
  } else {
    errorMessage.innerText = 'Потрібно ввести однакові значення';
  }
});

modalClose.addEventListener('click', () => {
  modal.style.display = 'none';
});

